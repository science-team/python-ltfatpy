ltfatpy.sigproc package - Signal processing tools
#################################################


General
=======

normalize
---------

.. automodule:: ltfatpy.sigproc.normalize
    :members:
    :undoc-members:
    :show-inheritance:

rms
---

.. automodule:: ltfatpy.sigproc.rms
    :members:
    :undoc-members:
    :show-inheritance:

Window functions
================

fir2long
--------

.. automodule:: ltfatpy.sigproc.fir2long
    :members:
    :undoc-members:
    :show-inheritance:

long2fir
--------

.. automodule:: ltfatpy.sigproc.long2fir
    :members:
    :undoc-members:
    :show-inheritance:

firkaiser
---------

.. automodule:: ltfatpy.sigproc.firkaiser
    :members:
    :undoc-members:
    :show-inheritance:

firwin
------

.. automodule:: ltfatpy.sigproc.firwin
    :members:
    :undoc-members:
    :show-inheritance:


Thresholding methods
====================

thresh
------

.. automodule:: ltfatpy.sigproc.thresh
    :members:
    :undoc-members:
    :show-inheritance:

groupthresh
-----------

.. automodule:: ltfatpy.sigproc.groupthresh
    :members:
    :undoc-members:
    :show-inheritance:    

largestn
--------

.. automodule:: ltfatpy.sigproc.largestn
    :members:
    :undoc-members:
    :show-inheritance:

largestr
--------

.. automodule:: ltfatpy.sigproc.largestr
    :members:
    :undoc-members:
    :show-inheritance:

..
	Module contents
	===============
	
	.. automodule:: ltfatpy.sigproc
	    :members:
	    :undoc-members:
	    :show-inheritance:
